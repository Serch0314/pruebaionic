import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactoItem } from '../models/contacto-item.model';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {

  contacto: ContactoItem;

  constructor( public contactosService: ContactosService, private route: ActivatedRoute ) {
    const contactoId = Number(this.route.snapshot.paramMap.get('listaId'));
    this.contacto = this.contactosService.obtenerContacto(contactoId);
  }

  ngOnInit() {
  }

}
