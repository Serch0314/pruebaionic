export class ContactoItem
{
    id: number;
    nombre: string;
    correo: string;
    telefono: string;
    fechaNacimiento: Date;

    constructor( nombre: string, correo: string, telefono: string, fechaNacimiento: Date )
    {
        this.id = new Date().getTime();
        this.nombre = nombre;
        this.correo = correo;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
    }
}