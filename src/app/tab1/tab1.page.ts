import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ContactoItem } from '../models/contacto-item.model';
import { ContactosService } from '../services/contactos.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  fecha = '';
  constructor( public contactosService: ContactosService,
               private router: Router,
               private alertController: AlertController
             ) 
  {
  }

  async agregarContacto()
  {
    //this.router.navigateByUrl('/tabs/tab1/agregar');
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Nuevo Contacto',
      inputs: [
        {
          name: 'Nombre',
          type: 'text',
          placeholder: 'Nombre de contacto'
        },
        {
          name: 'Correo',
          type: 'email',
          placeholder: 'Correo del contacto'
        },
        {
          name: 'Telefono',
          type: 'text',
          placeholder: 'Telefono del contacto'
        },
        {
          name: 'FechaN',
          type: 'date',
          placeholder: 'Fecha de Nacimiento'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () =>
          {
            console.log('cancelar');
          }
        },
        {
          text: 'Crear',
          handler: ( data ) =>
          {
            console.log(data);
            if ( data.Nombre.length > 0 )
            {
              const contactoN = this.contactosService.crearContacto(data.Nombre, data.Correo, data.Telefono, data.FechaN);
              //this.router.navigateByUrl(`tabs/tab1/editar/${ contactoN }`);
            }
          }
        },
      ]
    });
    alert.present();
  }

  borrar( i: number )
  {
    this.contactosService.contactos.splice(i,1);
    this.contactosService.guardarStorage();
  }

  async editar( contact: ContactoItem )
  {
    let date = new Date(contact.fechaNacimiento)

    let day = date.getDate()+1;
    let month = date.getMonth() + 1
    let year = date.getFullYear()
    if(month < 10){
      if(day < 10)
      {
        this.fecha = `${year}-0${month}-0${day}`;
      }
      else
      {
        this.fecha = `${year}-0${month}-${day}`;
      }
     
    }else
    {
      if(day < 10)
      {
        this.fecha = `${year}-${month}-0${day}`;
      }
      else
      {
        this.fecha = `${year}-${month}-${day}`;
      }
    }
    console.log(this.fecha);
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Editar Contacto',
      inputs: [
        {
          name: 'Nombre',
          type: 'text',
          placeholder: 'Nombre de contacto',
          value: contact.nombre,
        },
        {
          name: 'Correo',
          type: 'email',
          placeholder: 'Correo del contacto',
          value: contact.correo,
        },
        {
          name: 'Telefono',
          type: 'text',
          placeholder: 'Telefono del contacto',
          value: contact.telefono,
        },
        {
          name: 'FechaN',
          type: 'date',
          placeholder: 'Fecha de Nacimiento',
          value: this.fecha,
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () =>
          {
            console.log('cancelar');
          }
        },
        {
          text: 'Actualizar',
          handler: ( data ) =>
          {
            console.log(data);
            if ( data.Nombre.length > 0 )
            {
              contact.nombre = data.Nombre;
              contact.correo = data.Correo;
              contact.telefono = data.Telefono;
              contact.fechaNacimiento = data.FechaN;
              this.contactosService.guardarStorage();
            }
          }
        },
      ]
    });
    alert.present();
  }
}
