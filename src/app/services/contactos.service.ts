import { Injectable } from '@angular/core';
import { ContactoItem } from '../models/contacto-item.model';

@Injectable({
  providedIn: 'root'
})
export class ContactosService {

  public contactos: ContactoItem[] = [];

  constructor() 
  { 
    this.cargarStorage();
  }

  crearContacto (nombre: string, correo: string, telefono: string, fecha: string)
  {
    const contactoN = new ContactoItem(nombre, correo, telefono, new Date(fecha));
    this.contactos.push(contactoN);
    this.guardarStorage();
    return contactoN.id;
  }

  obtenerContacto( id: number )
  {
    id = Number(id);
    console.log(id);
    return this.contactos.find( contact => contact.id == id);
  }

  guardarStorage()
  {
    localStorage.setItem('data', JSON.stringify(this.contactos));
  }

  cargarStorage()
  {
    if( localStorage.getItem('data'))
    {
      this.contactos = JSON.parse(localStorage.getItem('data'));
    }
    else
    {
      this.contactos = [];
    }
    
  }
}
